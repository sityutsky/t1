import {storeMutations} from './Actions'

let mutations = storeMutations
let actions = Object.keys(mutations).reduce(
    (obj, k)=>{
        obj[k] = function(ctx, args){
            ctx.commit(k, args)
        }
        return obj
    },
    {}
)

export let storeConf = {
        state: {
            selected: -1,
            containers: [],
            nextContainerId: 0
        },
        mutations: mutations,
        actions: actions
    }