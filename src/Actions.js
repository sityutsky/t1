function deselect(state){
    if(state.selected != -1){
        state.containers[state.selected].active = false
        state.selected = -1
    }
}

function freeURLs(content){
    if(['image', 'video', 'audio'].includes(content.type)){
        URL.revokeObjectURL(content.data.src)
    }
}

function makeURLs(content){
    if(['image', 'video', 'audio'].includes(content.type)){
        if(content.data.file){
            content.data.src = URL.createObjectURL(content.data.file)
        }
    }
}

export let storeMutations = {
    addContainer(state, args){
        makeURLs(args.content)
        if(args.id == undefined){
            args.id = state.nextContainerId++
        }
        let container = {
            id: args.id,
            x: args.x,
            y: args.y,
            w: args.w,
            h: args.h,
            active: false,
            content: args.content
        }
        if (!('idx' in args)){
            args.idx = state.containers.length
        }
        state.containers.splice(args.idx, 0, container)
    },
    removeContainer(state, idx){
        freeURLs(state.containers[idx].content)
        state.selected = -1
        state.containers.splice(
            idx, 1
        )
    },
    changeContainer(state, args){
        if(args.prop == 'content'){
            freeURLs(state.containers[args.i].content)
            makeURLs(args.value)
        }
        
        state.containers[args.i][args.prop] = args.value
    },
    select(state, idx){
        if(state.selected != idx){
            deselect(state)
            state.containers[idx].active = true
            state.selected = idx
        }
    },
    deselect: deselect,
}

function oppositeAction(act, prev_state){
    let unact = []
    if(act.type == 'select' || act.type == 'deselect'){
        if(prev_state.selected == -1){
            unact.push({
                type: 'deselect',
                payload: {}
            })
        }else{
            unact.push({
                type: 'select',
                payload: prev_state.selected
            })
        }
    }else if(act.type == 'changeContainer'){
        unact.push({
            type: 'changeContainer',
            payload: {
                i: act.payload.i,
                prop: act.payload.prop,
                value: prev_state.containers[act.payload.i][act.payload.prop]
            }
        })
    }else if(act.type == 'addContainer'){
        unact.push({
            type: 'removeContainer',
            payload: 'idx' in act.payload ? act.payload.idx : prev_state.containers.length
        })
    }else if(act.type == 'removeContainer'){
        let container = prev_state.containers[act.payload]
        let content = {...container.content}
        if(content.type == 'video'){
            content = {
                ...content, 
                data: { 
                    src: null, 
                    files: content.data.files
                }
            }
        }
        unact.push({
            type: 'addContainer',
            payload: {
                x: container.x,
                y: container.y,
                w: container.w,
                h: container.h,
                idx: act.payload,
                content: content
            }
        })
        if(prev_state.selected == act.payload){
            unact.push({
                type: 'select',
                payload: act.payload
            })
        }
    }else{
        throw Error('cannot create opposite action')
    }
    return unact
}

export class Action{
    constructor(store, action, prev_state){
        if(action.type == 'select' || action.type == 'deselect'){
            this.category = 'selection'
        }else if(action.type == 'changeContainer'){
            if(['x', 'y'].includes(action.payload.prop)){
                this.category = 'moving'
            }else if(['w', 'h'].includes(action.payload.prop)){
                this.category = 'resizing'
            }else if(action.payload.prop == 'content'){
                this.category = 'content'
            }
            this.container = action.payload.i
        }else if(action.type == 'addContainer'){
            this.category = 'adding'
        }else if(action.type == 'removeContainer'){
            this.category = 'removing'
        }
        this.store = store
        this.acts = [action]
        this.unacts = oppositeAction(action, prev_state)
    }
    do(){
        for(let a of this.acts){
            this.store.dispatch(a.type, a.payload)
        }
    }
    undo(){
        for(let a of this.unacts){
            this.store.dispatch(a.type, a.payload)
        }
    }
    static consolidateActions(a, b){
        if(a.category == b.category){
            if(a.category == 'selection'){
                b.unacts = a.unacts
                return [b]
            }else if(
                (a.category == 'moving' || a.category == 'resizing') &&
                a.container == b.container
            ){
                let res = {}
                let what = ['acts', 'unacts']
                let order = [a, b]
                let props = a.category == 'moving' ? ['x', 'y'] : ['w', 'h']
                for(let i in what){
                    res[what[i]] = []
                    for(let prop of props){
                        let act = order[1-i][what[i]].find((e)=>(e.payload.prop == prop))
                        if(act === undefined){
                            act = order[i][what[i]].find((e)=>(e.payload.prop == prop))
                        }
                        if(act !== undefined){
                            res[what[i]].push(
                                {
                                    type: 'changeContainer',
                                    payload: {i:b.container, prop: prop, value: act.payload.value}
                                }
                            )
                        }
                    }
                }
                b.acts = res.acts
                b.unacts = res.unacts
                return [b]
            }else{
                return [a, b]
            }
        }else{
            return [a, b]
        }
    }
}