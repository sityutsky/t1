import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import {BootstrapVue, BootstrapVueIcons} from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'



Vue.use(Vuex)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.config.productionTip = false

import {storeConf} from './Store'
import {History, historyPlugin} from './History'

let history = new History()
Vue.prototype.$history = history

Vue.prototype.$eventHub = new Vue();

let store = new Vuex.Store({
  ...storeConf,
  plugins: [historyPlugin(history)]
})


new Vue({
  render: h => h(App),
  store
}).$mount('#app')
