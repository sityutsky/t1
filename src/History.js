import {Action} from './Actions'

export class History{
    constructor(){
        this.log = []
        this.pos = 0
        this.recording = true
    }
    action(store, act, prev_state){
        if(
            act.type == 'select' || 
            act.type == 'deselect'
        ){ 
            return
        }
        if(this.recording){
            let action = new Action(
                store,
                act,
                prev_state
            )
            if(this.pos != this.log.length){
                this.log.splice(this.pos, this.log.length - this.pos)
            }
            if(this.log.length > 0){
                let prev_action = this.log[this.log.length - 1]
                let actions = Action.consolidateActions(prev_action, action)
                this.log.splice(this.pos-1, 1, ...actions)
                this.pos += actions.length - 1
            }else{
                this.log.push(action)
                this.pos = 1
            }
        }
    }
    back(){
        this.recording = false
        if(this.pos > 0){
            this.pos--
            this.log[this.pos].undo()
        }
        this.recording = true
    }
    forward(){
        this.recording = false
        if(this.pos < this.log.length){
            this.log[this.pos].do()
            this.pos++
        }
        this.recording = true
    }
    save(){
        this.log.splice(0, this.pos)
        this.pos = 0
    }
}

export let historyPlugin = function(history){
    return function(store){
        store.subscribeAction({
            before(action, prev_state){
                history.action(store, action, prev_state)
            }
        })
    }
}